from django.contrib import admin
from .models import Question, Choice

# Register your models here.

#admin.site.siteheader = "Pollster Admin"
#admin.site.site_title = "Pollster Admin Area"
#admin.site.index_title = "Welcome to the pollster admin area"

#class ChoiceInline(admin.TabularInline):
 #   model = Choice
  #  extra = 3

#class QuestionAdmin(admin.ModelAdmin):
 #   fieldsets = [(None, {'fields': ['question_text']}),
  #               ('Date Information', {'fields': ['pub_date'], 'classes': ['collapse']}),]
   # Inlines = [ChoiceInline]
#1

#admin.site.register(Question, QuestionAdmin, Choice)
admin.site.register(Choice)
admin.site.register(Question)