from django.urls import path
from .views import QuestionList, ChoiceList

urlpatterns = [
    path('questions/', QuestionList.as_view(), name='questions-list'),
    path('choices/', ChoiceList.as_view(), name='choice-list')
   # path('', views.index, name='index'),
    #path('<int:question_id>/', views.detail, name='detail'),
    #path('<int:question_id>/results/', views.results, name='results'),
    #path('<int:question_id>/vote/', views.vote, name='vote'),
]