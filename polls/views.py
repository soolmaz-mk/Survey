from django.shortcuts import render

# Create your views here.
from django.http import HttpResponse
from .models import Question, Choice
from .serializers import QuestionSerializer, ChoiceSerializer
from rest_framework import generics
from rest_framework.permissions import IsAdminUser

class QuestionList(generics.ListCreateAPIView):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer


class ChoiceList(generics.ListCreateAPIView):
    queryset = Choice.objects.all()
    serializer_class = ChoiceSerializer

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:10]
    output = ','.join([q.question_text for q in latest_question_list])
    return HttpResponse(output)

def detail(request, question_id):
    return HttpResponse("You're looking at question %s." %question_id)

def results(request, question_id):
    response = "You are looking at the results of question %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)
